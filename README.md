# Database access API used by IOCBIO projects

This is used internally by IOCBIO Sparks and IOCBIO Kinetics to access
databases. It is based on SQLAlchemy Python package. In addition,
`iocbio.db` has some other components making integration into Sparks
and Kinetics simple to use.

